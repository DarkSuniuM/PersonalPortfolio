// TODO Change Font Sizes from Pixel to EM

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import normalize from 'normalize.css'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faCoffee, faAt, faPhone} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {
  faLinkedin,
  faInstagram,
  faGitlab,
  faTelegram,
  faStackOverflow,
  faGithub,
  faMedium
} from '@fortawesome/free-brands-svg-icons'


library.add(
  faCoffee,
  faAt,
  faPhone,
  faLinkedin,
  faInstagram,
  faGitlab,
  faTelegram,
  faStackOverflow,
  faGithub,
  faMedium)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
